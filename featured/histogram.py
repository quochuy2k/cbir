from cv2 import cv2 as cv
class Histogram:
    def __init__(self, histSize, ranges, channels):
        self.histSize = histSize
        self.ranges = ranges
        self.channels = channels

    def describe(self, image):
        histSize = self.histSize
        channels = self.channels
        ranges = self.ranges
        hist_base = cv.calcHist([image], channels, None,
                                histSize, ranges, accumulate=False)
        return hist_base


