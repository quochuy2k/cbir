

RUN SYSTEM

file configDataFolder không cần phải chạy vì đây là file để tác giả cấu hình thư mục ảnh. Thư mục testing chứa kho ảnh test, training để cho hệ thống học, query chứa các ảnh để truy vấn
câu lệnh chạy:  python configDataFolder.py --training static/training --testing static/testing --query static/query  

b1: * Lưu các đặc trưng vào database ( mongoo) - trước đó tạo database cbir trong mongoo db trước đã (có thể dùng mongoo compass)
      câu lệnh : python connectDb.py --training static/training --testing static/testing
      khi hệ thống thông báo success thì ok.
b2: * chạy file app.py
      câu lệnh: python app.py
      khi hệ thống thông báo server ready !! thì ok
b3: * mở brower truy cập vào localhost:5000
b4: * Chọn ảnh truy vấn - chú ý chọn đúng ảnh trong thư mục static/query của hệ thống
b5: * Nhấn truy vấn và sẽ nhìn thấy kết quả được hiển thị

SYSTEM DEGIGN

Hệ thống sử dụng các đặc trưng sau cả ảnh:
- LBP
- Histogram
- Hai đặc trưng trên đều sử dụng trên ảnh đa mức xám

Sau khi trích chọn đặc trưng thì sử dụng LinearSVC để phân loại
Các ảnh trong thư mục training chính là để training cho SVC

Chọn ảnh truy vấn,sau đó trích đặc trưng của ảnh sau đó phân loại ảnh đó, so sánh hisrogram dùng compareHist, những ảnh trong test nếu cùng loại và compare > 0.9  thì show ra.

author: vũ quốc huy