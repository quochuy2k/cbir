from numpy.lib.function_base import append
import pymongo
from cv2 import cv2 as cv
from featured.localbinarypatterns import LocalBinaryPatterns
from featured.histogram import Histogram
import numpy as np
import argparse
from imutils import paths
from bson.binary import Binary
import pickle
import os
ap = argparse.ArgumentParser()
ap.add_argument("-t", "--training", required=True,
                help="path to the training images")
ap.add_argument("-e", "--testing", required=True,
                help="path to the tesitng images")
args = vars(ap.parse_args())
desc = LocalBinaryPatterns(24, 8)
histogram = Histogram([256], [0, 256], [0])
data_training = []
labels_training = []
data_hist_training = []
path_testing = []
data_testing = []
data_hist_testing = []
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
mydb = myclient["cbir"]
mycoltraining = mydb["data-training"]
mycoltesting = mydb["data-testing"]
leng = paths.list_images(args["training"])
k = 0
for imagePath in paths.list_images(args["training"]):
    image = cv.imread(imagePath)
    gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    hist = desc.describe(gray)
    histog = histogram.describe(gray).flatten()
    labels_training.append(imagePath.split(os.path.sep)[-1].split("__")[0])
    data_training.append(hist)
    data_hist_training.append(histog)
for imagePath in paths.list_images(args["testing"]):
    image = cv.imread(imagePath)
    gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    hist = desc.describe(gray)
    histog = histogram.describe(gray).flatten()
    path_testing.append(imagePath)
    data_testing.append(hist)
    data_hist_testing.append(histog)


traning = {
    "lbp": Binary(pickle.dumps(np.array(data_training), protocol=2), subtype=128),
    "histogram": Binary(pickle.dumps(np.array(data_hist_training), protocol=2), subtype=128),
    "labels": labels_training
}
testing = {
    "path": path_testing,
    "lbp": Binary(pickle.dumps(np.array(data_testing), protocol=2), subtype=128),
    "histogram": Binary(pickle.dumps(np.array(data_hist_testing), protocol=2), subtype=128)
}
mycoltraining.insert_one(traning)
mycoltesting.insert_one(testing)

print("success!!")
