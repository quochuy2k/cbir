from flask import Flask, render_template, request
from featured.localbinarypatterns import LocalBinaryPatterns
from featured.histogram import Histogram
from gevent.pywsgi import WSGIServer
import pymongo
from sklearn.svm import LinearSVC
import pickle
import time
from cv2 import cv2 as cv
import os
app = Flask(__name__)

model = LinearSVC(C=100.0, random_state=42)
modelHist = LinearSVC(C=100.0, random_state=42)
modelHog = LinearSVC(C=100.0, random_state=42)
src = []
lbp_test = []
hist_test = []
x = 0
label = []
hist = []
lbp = []
c = 0


@app.route('/')
def main():
    return render_template('homepage.html')


@app.route('/api', methods=['POST', 'GET'])
def findImage():
    if(request.method == 'POST'):
        start_time = time.time()
        desc = LocalBinaryPatterns(24, 8)
        histogram = Histogram([256], [0, 256], [0])
        counts = 0
        path_image = request.form.get('image_path')
        image = cv.imread(path_image)
        gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
        histlbp = desc.describe(gray)
        histog = histogram.describe(gray).flatten()
        pre1 = model.predict(histlbp.reshape(1, -1))
        pre2 = modelHist.predict(histog.reshape(1, -1))
        result_path = ''
        for i in range(0, x):
            k = cv.compareHist(histog, hist_test[i], 0)
            pred_lbp = model.predict(lbp_test[i].reshape(1, -1))
            pred_hist = modelHist.predict(hist_test[i].reshape(1, -1))
            if (pred_lbp[0] == pre1[0] and pred_hist[0] == pre2[0] and k > 0.8) or (pred_lbp[0] == pre1[0] and k > 0.97):
                path = "../static/testing/"+src[i].split(os.path.sep)[-1]
                result_path = result_path + path + " "
                counts = counts+1
            if counts==10:
                break
        end_time = str(time.time()-start_time)
        return result_path+' '+end_time


if __name__ == '__main__':
    myclient = pymongo.MongoClient("mongodb://localhost:27017/")
    mydb = myclient["cbir"]
    mycoltraining = mydb["data-training"]
    mycoltesting = mydb["data-testing"]
    training = mycoltraining.find_one()
    testing = mycoltesting.find_one()
    label = training['labels']
    lbp = pickle.loads(training['lbp'])
    hist = pickle.loads(training['histogram'])
    model.fit(lbp, label)
    modelHist.fit(hist, label)
    result_path = []
    src = testing['path']
    lbp_test = pickle.loads(testing['lbp'])
    hist_test = pickle.loads(testing['histogram'])
    x = len(src)
    print("Server ready !!")
    http_server = WSGIServer(('', 5000), app)
    http_server.serve_forever()
