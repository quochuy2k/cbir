from imutils import paths
import argparse
import os
from cv2 import cv2 as cv
ap = argparse.ArgumentParser()
ap.add_argument("-t", "--training", required=True,
                help="path to the training images")
ap.add_argument("-e", "--testing", required=True,
                help="path to the tesitng images")
ap.add_argument("-q", "--query", required=True,
                help="path to the tesitng images")
args = vars(ap.parse_args())
for imagePath in paths.list_images(args["testing"]):
    t = imagePath.split(os.path.sep)[-1].split("__")[1]
    if t=='0.png' or t == '5.png' or t == '10.png' or t == '15.png' or t == '20.png' or t == '25.png' or t == '30.png' or t == '35.png' or t == '40.png' or t == '45.png' or t == '50.png' or t == '55.png'or t == '60.png' or t == '65.png' or t == '70.png' or t == '75.png' or t == '80.png'or t == '85.png' or t == '90.png' or t == '95.png' or t == '100.png':
        os.remove(imagePath)
for imagePath in paths.list_images(args["training"]):
    t = imagePath.split(os.path.sep)[-1].split("__")[1]
    if not(t == '55.png' or t == '5.png' or t == '10.png' or t == '15.png' or t == '20.png' or t == '25.png' or t == '30.png' or t == '35.png' or t == '40.png' or t == '45.png' or t == '50.png'or t == '60.png' or t == '65.png' or t == '70.png' or t == '75.png' or t == '80.png'or t == '85.png' or t == '90.png' or t == '95.png' or t == '100.png'):
        os.remove(imagePath)
for imagePath in paths.list_images(args["query"]):
    t = imagePath.split(os.path.sep)[-1].split("__")[1]
    if not(t == '0.png'):
        os.remove(imagePath)
print("success")
